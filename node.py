class Node:
    def __init__(self):
        self.attributeLabel = -1
        self.children = {}
        self.value = -float("inf")
        self.mostCommunClass = ""

    def addChild(self, element, child):
        self.children[element] = child

    def isLeaf(self):
        return len(self.children) == 0