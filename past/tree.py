import math
import random
import sys

# Classe de l'arbre
class Tree:
    prediction = ""
    classes = {}
    children = {}
    father = None
    test = -1
    infoGain = 0.0

    def __init__(self):
        self.children = {}

    def addChild(self, name, child):
        if name not in self.children:
            self.children[name] = child
            self.children[name].father = self
        return self.children[name]

    def removeChild(self, name):
        del self.children[name]

    def getChild(self, name):
        return self.children[name]

    def setClassValue(self, name, value):
        self.classes[name] = value 

    def removeClass(self, name):
        del self.classes[name]

    def isLeaf(self):
        if len(self.children) == 0:
            return True
        return False

# Fonction de construction de l'arbre
# I : dataset
# gainFunction : fonction de gain a utiliser
# forbidenTests : liste contenant les attributs deja tester dans les noeuds precedents
def BuildTree(I, gainFunction, depth, forbidenTests):
    classes = getClasses(I)
    t = Tree()
    t.classes = classes
    t.prediction = getPrediction(classes)

    attr = split(I, gainFunction, forbidenTests)
    if not worthSplittin(t, depth, attr[0]):
        return t

    t.test = attr[0]
    forbidenTests.append(t.test)
    t.infoGain = attr[1]

    values = getAttributeValues(I, t.test)
    if isinstance(values[0], tuple):
        ISort = getISorted(I, t.test)
        binf = -math.inf

        cPrev = ISort[0][0]

        n = len(ISort)
        for i in range(n):
            c = ISort[i][0]
            if c != cPrev or i == n - 1:
                if i == n - 1:
                    bsup = math.inf
                else:
                    bsup = (ISort[i][t.test] + ISort[i - 1][t.test]) / 2

                IChild = getIChildContinu(I, t.test, binf, bsup)
                t.addChild((binf, bsup), BuildTree(IChild, gainFunction, depth - 1, forbidenTests))
                binf = bsup
            cPrev = c
    else:
        for value in values:
            IChild = getIChild(I, t.test, value)
            t.addChild(value, BuildTree(IChild, gainFunction, depth - 1, forbidenTests))
    return t

# Retourne la prediction
def getPrediction(classes):
    m = 0.0
    for elt in classes:
        if classes[elt] > m:
            eltmax = elt
            m = classes[elt]
    return eltmax

# Retourne l'attribut a tester et la valeur du gain correspondant
def split(I, gainFunction, forbidenTests):
    attr = -1
    maxInfoGain = 0.0

    for i in range(1, len(I[0])):
        if i not in forbidenTests:
            classesList = {}

            attrValues = getAttributeValues(I, i)
            for attrValue in attrValues:
                if isinstance(attrValue, tuple):
                    IChild = getIChildContinu(I, i, attrValue[0], attrValue[1])
                else:
                    IChild = getIChild(I, i, attrValue)
                classesList[attrValue] = getClasses(IChild);

            tmpGain = gainFunction(classesList)
            if maxInfoGain < tmpGain:
                maxInfoGain = tmpGain
                attr = i

    return [attr, maxInfoGain]

# Calcul le gain d'information selon le parametre "classesList"
def computeInfoGain(classesList):
    classSum = 0.0
    for attrValue in classesList:
        for c in classesList[attrValue]:
            classSum += classesList[attrValue][c]

    res = 0.0
    for a in classesList:
        probA = 0.0
        for cl in classesList[a]:
            probA += classesList[a][cl]
        probA /= classSum

        for c in classesList[a]:
            probC = 0.0
            for at in classesList:
                if c in classesList[at]:
                    probC += classesList[at][c]
            probC /= classSum

            probAC = classesList[a][c] / classSum

            res += probAC * math.log(probAC / (probA * probC), 2)

    return res

# Calcul le ration gain selon le parametre "classesList"
def computeRatioGain(classesList):
    classSum = 0.0
    for attrValue in classesList:
        for c in classesList[attrValue]:
            classSum += classesList[attrValue][c]

    splitInfo = 0.0
    for attrValue in classesList:
        nbExamples = 0
        for c in classesList[attrValue]:
            nbExamples += classesList[attrValue][c]
        splitInfo -= (nbExamples / classSum) * math.log(nbExamples / classSum, 2)

    if splitInfo == 0:
        return 0

    gain = 0.0
    for a in classesList:
        probA = 0.0
        for cl in classesList[a]:
            probA += classesList[a][cl]
        probA /= classSum

        for c in classesList[a]:
            probC = 0.0
            for at in classesList:
                if c in classesList[at]:
                    probC += classesList[at][c]
            probC /= classSum

            probAC = classesList[a][c] / classSum

            gain += probAC * math.log(probAC / (probA * probC), 2)

    return gain / splitInfo

# Décide si le noeud vaut le coup d'etre separer
def worthSplittin(depth, attr):
    return depth != 0 and attr != -1

# Retourne un dict avec le nom des classes et le nombre d'occurences correspondant
def getClasses(I):
    classes = {}
    for pattern in I:
        if pattern[0] in classes:
            classes[pattern[0]] += 1
        else:
            classes[pattern[0]] = 1
    return classes

# Retourne les valeurs des classes
def getClassesValues(I):
    values = []
    for pattern in I:
        if not pattern[0] in values:
            values.append(pattern[0])
    return values

# Retourne les valeurs de l'attribut "attr" dans le dataset "I"
def getAttributeValues(I, attr):
    values = []
    if isinstance(I[0][attr], float):
        ISort = getISorted(I, attr)
        binf = -math.inf

        cPrev = ISort[0][0]

        n = len(ISort)
        for i in range(n):
            c = ISort[i][0]
            if c != cPrev or i == n - 1:
                if i == n - 1:
                    bsup = math.inf
                else:
                    bsup = (ISort[i][attr] + ISort[i - 1][attr]) / 2

                values.append((binf, bsup))
                binf = bsup
            cPrev = c
    else:
        for pattern in I:
            if not pattern[attr] in values:
                values.append(pattern[attr])
    return values

# Retourne le dataset dans lequel l'attribut "attr" a la valeur "value"
def getIChild(I, attr, value):
    result = []
    for pattern in I:
        if value == pattern[attr]:
            result.append(pattern)
    return result

# Trie le dataset "I" selon les valeurs de l'attribut "attr" (pour les attributs continus)
def getISorted(I, attr):
    for i in range(len(I)):
        index = i
        for j in range(i + 1, len(I)):
            if I[j][attr] < I[index][attr]:
                index = j
        if index != i:
            tmp = I[index]
            I[index] = I[i]
            I[i] = tmp
    return I

# Retourne le dataset dans lequel les valeurs de l'attribut "attr" est compris entre binf et bsup (attributs continus)
def getIChildContinu(I, attr, binf, bsup):
    result = []
    for pattern in I:
        if binf <= pattern[attr] and pattern[attr] < bsup:
            result.append(pattern)
    return result

def test(t, x):
    if t.isLeaf() or (not x[t.test] in t.children and not isinstance(x[t.test], float)):
        return t.prediction

    if isinstance(x[t.test], float):
        for bounds in t.children:
            if bounds[0] <= x[t.test] and x[t.test] < bounds[1]:
                return test(t.children[bounds], x)
    else:
        return test(t.children[x[t.test]], x)

# Fonction d'ecriture de l'arbre
# columns : noms des attributs
# gain : string representant le gain utilise
#
# L'arbre est ecris sous forme de graphe oriente au format dot
def printDotTree(t, f, columns, gain):
    f.write('digraph g {\n')
    recPrintDotTree(t, f, columns, '', gain)
    f.write('}\n')

def recPrintDotTree(t, f, columns, transition, gain):
    if t.isLeaf():
        f.write(repr(t) + ' [shape=box, style=filled, color=black, fillcolor=green, label=\"-----\n')
        for c in t.classes:
            f.write(c + ': ' + repr(t.classes[c]) + '\n')
        f.write(gain + repr(round(t.infoGain, 4)) + '\"]\n')
        if t.father != None:
            f.write(repr(t.father) + ' -> ' + repr(t) + '[label=\"' + repr(transition) + '\"]\n')
    else:
        f.write(repr(t) + ' [shape=box, label=\"' + columns[t.test] + '\n')
        for c in t.classes:
            f.write(c + ': ' + repr(t.classes[c]) + '\n')
        f.write(gain + repr(round(t.infoGain, 4)) + '\"]\n')

        if t.father != None:
            f.write(repr(t.father) + ' -> ' + repr(t) + '[label=\"' + repr(transition) + '\"]\n')
        for child in t.children:
            recPrintDotTree(t.children[child], f, columns, child, gain)

# Evalue l'arbre construit a partir de "trainSet" sur "testSet"
def evaluate(trainSet, testSet, gainFunction, depth):
    tree = BuildTree(trainSet, gainFunction, depth, [])

    classesValue = getClassesValues(trainSet)

    TP = 0
    FP = 0
    TN = 0
    FN = 0

    for x in testSet:
        result = test(tree, x)
        if result == x[0]:
            if result == classesValue[0]:
                TP += 1
            else:
                TN += 1
        else:
            if result == classesValue[0]:
                FP += 1
            else:
                FN += 1

    recall = TP / (TP + FN)
    miss = FN / (TP + FN)

    if FP + TN == 0:
        FAR = 1.0
    else:
        FAR = FP / (FP + TN)
    specificity = 1 - FAR
    precision = TP / (TP + FP)
    f_score = recall * precision
    succeed = (TP + TN) / (TP + FP + TN + FN)

    print("Recall = " + repr(round(recall, 3)))
    print("FAR = " + repr(round(FAR, 3)))
    print("Miss = " + repr(round(miss, 3)))
    print("Specificity = " + repr(round(specificity, 3)))
    print("Precision = " + repr(round(precision, 3)))
    print("F-score = " + repr(round(f_score, 3)))
    print("Succeed = " + repr(round(succeed, 3)))

    return [recall, FAR, miss, specificity, precision, f_score, succeed]

# Effectue une "nbFold" cross-validation sur le "dataSet"
def crossValidation(nbFold, dataSet, gainFunction):
    n = len(dataSet)
    results = [0, 0, 0, 0, 0, 0, 0]
    for i in range(nbFold):
        sep1 = int(i * n / nbFold)
        sep2 = int((i + 1) * n / nbFold)

        trainSet = dataSet[0:sep1]
        trainSet.extend(dataSet[sep2:])
        testSet = dataSet[sep1:sep2]

        if sep1 == 0:
            print("Train set : pattern " + repr(sep2) + " to pattern " + repr(n))
        elif sep2 == n:
            print("Train set : pattern 0 to pattern " + repr(sep1))
        elif sep1 != 0 and sep2 != n:
            print("Train set : pattern 0 to pattern " + repr(sep1) + " and pattern " + repr(sep2) + " to pattern " + repr(n))
        print("Test set : pattern " + repr(sep1) + " to pattern " + repr(sep2))
        print()

        tmpResults = evaluate(trainSet, testSet, gainFunction, 10)
        for i in range(len(results)):
            results[i] += tmpResults[i]

        print("----------")

    print("Overall results :")
    print("Recall = " + repr(round(results[0] / nbFold, 3)))
    print("FAR = " + repr(round(results[1] / nbFold, 3)))
    print("Miss = " + repr(round(results[2] / nbFold, 3)))
    print("Specificity = " + repr(round(results[3] / nbFold, 3)))
    print("Precision = " + repr(round(results[4] / nbFold, 3)))
    print("F-score = " + repr(round(results[5] / nbFold, 3)))
    print("Succeed = " + repr(round(results[6] / nbFold, 3)))

def main():
    if len(sys.argv) >= 3:
        content = []
        for line in open(sys.argv[1]):
            example = line[:-1].split(',')
            for i in range(len(example)):
                if '0' <= example[i][0] and example[i][0] <= '9':
                    example[i] = float(example[i])
            content.append(example)

        columns = content[0]

        # Selection de la function de gain
        gainFunction = computeInfoGain
        gainPrint = 'Info Gain: '
        if len(sys.argv) == 3 and sys.argv[3] == '-ratiogain':
            gainFunction = computeRatioGain
            gainPrint = 'Ratio Gain: '
        t = BuildTree(content[1:], gainFunction, 8, [])

        f = open(sys.argv[2], 'w')
        printDotTree(t, f, columns, gainPrint)
        f.close()

        dataSet = content[1:]
        if len(dataSet) > 100:
            random.shuffle(dataSet)
            crossValidation(5, dataSet, gainFunction)
    else:
        print('Usage: python tree.py dataset.txt tree.dot [-ratiogain]')

if __name__ == '__main__':
    main()