import sys
from math import log
from random import randrange
from random import shuffle
from random import randint
from sklearn.decomposition import PCA

from node import *
from mnist import *

###
### GLOBAL
###

comparisonFunctions = {"<=": lambda a, b: a <= b, ">": lambda a, b: a > b}

baggedSample = 0.5

dataSeparation = 0.7

maximumDepth = 100

numberOfTrees = 40

minimumNodeSizeRatio = 0.001

###
### GAIN FUNCTIONS
###

def entropy(Y):
    total = 0
    for e in set(Y):
        pyi = len([v for v in Y if v == e]) / float(len(Y))
        total += pyi * log(1 / pyi, 2)
    return total

def giniImpurity(Y):
    total = 1
    for e in set(Y):
        pyi = len([v for v in Y if v == e]) / float(len(Y))
        total -= pyi * pyi
    return total

criteriaFunction = entropy

def dataSeparationFloat(examples, attribute):
    sortedExamples, sortedAttribute = zip(*sorted(zip(examples, attribute)))

    lengthAttribute = len(attribute)

    bestGain = -0.01
    bestValue = -float("inf")

    for i in range(0, len(sortedAttribute)):
        total = float(i) / lengthAttribute * criteriaFunction(sortedExamples[:i])
        total += float(lengthAttribute - i) / lengthAttribute * criteriaFunction(sortedExamples[i:])
        if total < bestGain:
            bestGain = total
            bestValue = sortedAttribute[i]

    return criteriaFunction(examples) - bestGain, bestValue

def infoGain(examples, attribute):
    total = 0
    for element in set(attribute):
        examplesOfElement = []
        for i in range(0, len(examples)):
            if attribute[i] == element:
                examplesOfElement.append(examples[i])
        total += float(len(examplesOfElement)) / len(attribute) * criteriaFunction(examplesOfElement)
    return criteriaFunction(examples) - total

def infoGainFloat(examples, attribute):
    value = (sum(attribute) / len(attribute) + sorted(attribute)[int(len(attribute) / 2)]) / 2 
    total = 0
    for function in comparisonFunctions.keys():
        examplesOfElement = []
        for i in range(0, len(examples)):
            if comparisonFunctions[function](attribute[i], value):
                examplesOfElement.append(examples[i])
        total += float(len(examplesOfElement)) / len(attribute) * criteriaFunction(examplesOfElement)
    return criteriaFunction(examples) - total, value

def gainRatio(examples, attribute):
    splitInformation = 0
    for element in set(attribute):
        examplesOfElement = []
        for i in range(0, len(examples)):
            if attribute[i] == element:
                examplesOfElement.append(examples[i])
        splitInformation -= len(examplesOfElement) / len(attribute) * log(len(examplesOfElement) / len(attribute)) * criteriaFunction(examplesOfElement)
    return infoGain(examples, attributes) / splitInformation

def gainRatioFloat(examples, attribute):
    value = (sum(attribute) / len(attribute) + sorted(attribute)[int(len(attribute) / 2)]) / 2 
    splitInformation = 0
    for function in comparisonFunctions.keys():
        examplesOfElement = []
        for i in range(0, len(examples)):
            if comparisonFunctions[function](attribute[i], value):
                examplesOfElement.append(examples[i])
        splitInformation -= len(examplesOfElement) / len(attribute) * log(len(examplesOfElement) / len(attribute)) * criteriaFunction(examplesOfElement)
    return infoGain(examples, attributes) / splitInformation, value

def giniIndex(examples, attribute):
    pass

gainFunction = infoGain
gainFunctionFloat = infoGainFloat

###
### UTILS
###

def mostCommunClass(examples):
    return max(set(examples), key=examples.count)

def allExcept(l, i):
    return l[:i] + l[(i + 1):]

###
### C45
###

def C45(examples, attributes, attributeLabels, rl, mr, msn):
    root = Node()
    root.mostCommunClass = mostCommunClass(examples)
    if rl > mr or len(examples) < msn:
        root.attributeLabel = -1
        return root
    if len(set(examples)) == 1:
        root.attributeLabel = -1
    elif len(attributes) == 0:
        root.attributeLabel = -1
    else:
        maximumGain = -0.01
        idAttribute = -1
        attribute = []
        bestValue = -float("inf")
        bestExamplesCopy = []
        bestAttributesCopy = []
        for i in range(0, len(attributes)):
            examplesCopy = []
            attributesCopy = [[] for e in attributes]
            for j in range(0, len(attributes[i])):
                if (attributes[i][j] != "?"):
                    examplesCopy.append(examples[j])
                    for k in range(0, len(attributes)):
                        attributesCopy[k].append(attributes[k][j])

            if isinstance(attributes[i][0], float):
                actualGain, value = gainFunctionFloat(examplesCopy, attributesCopy[i])
            else:
                actualGain, value = gainFunction(examplesCopy, attributesCopy[i]), -float("inf")
            if actualGain > maximumGain:
                idAttribute = i
                maximumGain = actualGain
                bestValue = value
                bestExamplesCopy = examplesCopy
                bestAttributesCopy = attributesCopy

        if maximumGain < 0.1:
            root.attributeLabel = -1
            return root

        root.attributeLabel = attributeLabels[idAttribute]
        attribute = attributes[idAttribute]

        if bestValue != -float("inf"):
            root.value = bestValue
            for function in comparisonFunctions.keys():
                examplesOfElement = []
                attributesOfElement = [[] for i in range(0, len(attributes))]
                for i in range(0, len(attribute)):
                    if comparisonFunctions[function](attribute[i], bestValue):
                        examplesOfElement.append(examples[i])
                        [attributesOfElement[j].append(attributes[j][i]) for j in range(0, len(attributes))]

                root.addChild(function, C45(examplesOfElement, attributesOfElement, attributeLabels, rl + 1, mr, msn))
        else:
            for element in set(attribute):
                if element != "?":
                    examplesOfElement = []
                    attributesOfElement = [[] for i in range(0, len(attributes))]
                    for i in range(0, len(attribute)):
                        if attribute[i] == element:
                            examplesOfElement.append(examples[i])
                            [attributesOfElement[j].append(attributes[j][i]) for j in range(0, len(attributes))]

                    root.addChild(element, C45(examplesOfElement, allExcept(attributesOfElement, idAttribute), allExcept(attributeLabels, idAttribute), rl + 1, mr, msn))

    return root

###
### BUILDING
###

def buildBaggedDecisionTree(examples, attributes, numberTrees, mr):
    baglist = []
    for k in range(0, numberTrees):
        print("Building tree number " + str(k + 1) + " of " + str(numberTrees))
        examplesSample = []
        attributesSample = [[] for e in attributes]
        for j in range(1, int(len(examples) * baggedSample)):
            r = randrange(0, len(attributes[0]))
            examplesSample.append(examples[r])
            [attributesSample[i].append(attributes[i][r]) for i in range(0,len(attributes))]
        baglist.append(C45(examplesSample, attributesSample, list(range(0, len(attributes))), 1, mr, len(examples) * minimumNodeSizeRatio))
    return baglist

###
### TESTING
###

def classify(root, element):
    if root.isLeaf():
        return root.mostCommunClass
    elif element[root.attributeLabel] == "?":
        return root.mostCommunClass
    else:
        for child in root.children.keys():
            if root.value != -float("inf"):
                if comparisonFunctions[child](float(element[root.attributeLabel]), root.value):
                    return classify(root.children[child], element)
            else:
                if child == element[root.attributeLabel]:
                    return classify(root.children[child], element)

def testingDecisionTree(root, testingLabels, testingData):
    positif = 0
    for i in range(0, len(testingData)):
        if testingLabels[i] == classify(root, testingData[i]):
            positif += 1
    print(str(positif) + " / " + str(len(testingData))) 
    print("Error: " + str(1 - positif / len(testingData)))

def testingBaggedDecisionTree(baglist, testingLabels, testingData):
    positif = 0
    for i in range(0, len(testingData)):
        if testingLabels[i] == mostCommunClass([classify(baglist[j], testingData[i]) for j in range(0, len(baglist))]):
            positif += 1
    print(str(positif) + " / " + str(len(testingData))) 
    print("Error: " + str(1 - positif / len(testingData)))

###
### MAIN
###

def parseFile(filename):
    data = []
    f = open(filename, 'r')
    [data.append(l.strip().split(',')) for l in f]
    f.close()
    return [data[0][0], data[0][1:]], data[1:]

def parseTrainingData(parsedData):
    examples = []
    attributes = [[] for e in parsedData[0][1]]
    for e in parsedData:
        examples.append(e[0]) #example class
        for i in range(0,len(e[1])):
            if e[1][i][0] >= '0' and e[1][i][0] <= '9' or e[1][i][0] == '.':
                attributes[i].append(float(e[1][i]))
            else:
                attributes[i].append(e[1][i])
    return examples, attributes

def parseTrainingDataMnist(parsedData):
    examples = []
    attributes = [[] for e in parsedData[0][1]]
    for e in parsedData:
        examples.append(e[0]) #example class
        for i in range(0,len(e[1])):
            attributes[i].append(e[1][i])
    return examples, attributes

def main():
    bagging = False
    mnist = False

    for i in range(1, len(sys.argv)):
        if sys.argv[i] == "-bagging":
            bagging = True
        elif sys.argv[i] == "-infogain":
            gainFunction = infoGain
            gainFunctionFloat = infoGainFloat
        elif sys.argv[i] == "-gainratio":
            gainFunction = gainRatio
            gainFunctionFloat = gainRatioFloat
        elif sys.argv[i] == "-entropy":
            criteriaFunction = entropy
        elif sys.argv[i] == "-giniimpurity":
            criteriaFunction = giniImpurity
        elif sys.argv[i] == "-mnist":
            mnist = True
        else:
            print("Unknown option " + sys.argv[i] + ".")

    if len(sys.argv) >= 2:
        if mnist == True:
            images, labels = load_mnist()
            images.resize((60000, 784))
            (n, m) = images.shape

            print("Transforming data ...")

            pca = PCA(n_components=10)
            pca.fit(images)
            data = pca.transform(images).tolist()

            parsedData = []
            [parsedData.append([labels[i], data[i]]) for i in range(0, len(data))]
            examples, attributes = parseTrainingDataMnist(parsedData)

            print("Training DecisionTree ...")

            if bagging == False:
                root = C45(examples, attributes, list(range(0, len(attributes))), 1, maximumDepth, len(examples) * minimumNodeSizeRatio)
            else:
                baglist = buildBaggedDecisionTree(examples, attributes, numberOfTrees, maximumDepth)

            print("Testing DecisionTree ...")

            testingImages, testingLabels = load_mnist(dataset="testing")
            testingImages.resize((10000, 784))
            testingData = pca.transform(testingImages).tolist()

            if bagging == False:
                testingDecisionTree(root, testingLabels, testingData)
            else:
                testingBaggedDecisionTree(baglist, testingLabels, testingData)
        else:
            attributeNames, data = parseFile(sys.argv[1])

            # Designing data categories

            # categorieList = []
            # for attributeName in attributeNames[1]:
            #     value = input(attributeName + " categorie: ")
            #     if value == "C":
            #         categorieList.append(value)
            #     elif value == "D":
            #         categorieList.append(value)
            #     else:
            #         print("Error with categorie type.")
            #         sys.quit(0)

            # Organizing data

            shuffle(data)
            trainingData = []
            [trainingData.append([l[0], l[1:]]) for l in data[:int(len(data) * dataSeparation)]]
            testingData = []
            testingLabels = []
            [(testingData.append(l[1:]), testingLabels.append(l[0])) for l in data[int(len(data) * dataSeparation):]]
            examples, attributes = parseTrainingData(trainingData)

            # Using data
            if bagging == False:
                root = C45(examples, attributes, list(range(0, len(attributes))), 1, maximumDepth, len(examples) * minimumNodeSizeRatio)
                testingDecisionTree(root, testingLabels, testingData)
            else:
                baglist = buildBaggedDecisionTree(examples, attributes, numberOfTrees, maximumDepth)
                testingBaggedDecisionTree(baglist, testingLabels, testingData)

if __name__ == "__main__":
    main()
